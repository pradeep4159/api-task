import React from 'react';
import './App.css';
import Person from './Components/Person';
import {BrowserRouter as Router,Link,Route,Switch } from 'react-router-dom';
import PersonDetail from './Components/PersonDetail';


 class App extends React.Component{
   render(){
    return (
      <Router>
      <Switch>
        <Route path="/users" exact component={Person}/>
        <Route path="/users/:id" component={PersonDetail}/>
        
      </Switch>
      </Router>
    );
    }
   }
 

export default App;
