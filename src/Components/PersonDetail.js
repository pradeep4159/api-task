import React, { Component } from 'react'
import axios from 'axios';

 class PersonDetail extends Component {
    componentDidMount(){
        axios.get(`https://jsonplaceholder.typicode.com/posts/${this.props.match.params.id}`).then(res =>{
            console.log(res);
            this.setState({persons:res.data});
        });
  }
    render() {
        console.log(this.props);
        return (
            <div>
               <h1> {this.props.match.params.id}</h1>
            </div>
        )
    }
}
export default PersonDetail;