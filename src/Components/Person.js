import React from 'react';
import axios from 'axios';
import {Link}from 'react-router-dom';


class Person extends React.Component{
    state ={
        persons:[]
    }
    componentDidMount(){
          axios.get('https://jsonplaceholder.typicode.com/posts').then(res =>{
              console.log(res);
              this.setState({persons:res.data});
          });
    }
    render()
    {
        return(
          <ul>
                {this.state.persons.map(person => <div key={person.id} className="card" >
                  {person.id} --{person.title} 
                <Link to={`/users/${person.id}`}>  <div className="butn"><button>add</button></div></Link>
                </div>)}
          </ul>
        )
      
    }
}
export default Person;